FROM openjdk:8-jdk-alpine

ADD target/shopping-cart-service-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8003

ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=docker", "app.jar"]

# Maven
# mvn clean package -Dspring.profiles.active=docker -Dmaven.test.skip=true
# Build
# docker build -t nuts-cart .