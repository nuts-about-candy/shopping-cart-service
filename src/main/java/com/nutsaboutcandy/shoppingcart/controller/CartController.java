package com.nutsaboutcandy.shoppingcart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nutsaboutcandy.shoppingcart.entity.CartItem;
import com.nutsaboutcandy.shoppingcart.service.CartService;

@RestController
@RequestMapping("/cart")
public class CartController {
	
	@Autowired
	private CartService cartService;

	@GetMapping("/{id}")
	public ResponseEntity<Object> getCartItemsByUserId(@PathVariable("id") long id) {
		
		return ResponseEntity.ok().body(cartService.getCartItemsByUserId(id));
	}
	
	@PostMapping
	public ResponseEntity<Object> addToCart(@RequestBody CartItem cartItem){
		
		cartService.addToCart(cartItem);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Object> updateCartItem(@PathVariable("id") int cartId,
			@RequestBody CartItem cartItem){
		
		try {
			cartService.updateCartItem(cartId, cartItem);
		} catch(Exception e) {
			return new ResponseEntity<>("{\"message\":\""+ e.getMessage() +"\"}", HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok().build();
	}
	
	@PostMapping("/{id}/add-multiple")
	public ResponseEntity<Object> addMultipleItemsToCart(
			@PathVariable("id") int userId,
			@RequestBody List<CartItem> cartItems){
		
		cartService.addMultipleItemsToCart(userId, cartItems);
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> removeCartItemByCartId(@PathVariable("id") long id){
		
		try {
			cartService.removeCartItemByCartId(id);
		} catch(Exception e) {
			return new ResponseEntity<>("{\"message\":\""+ e.getMessage() +"\"}", HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok().build();
	}
	
	
}
