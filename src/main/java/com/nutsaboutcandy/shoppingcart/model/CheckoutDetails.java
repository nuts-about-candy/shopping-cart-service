package com.nutsaboutcandy.shoppingcart.model;

import java.util.List;

import com.nutsaboutcandy.shoppingcart.entity.CartItem;

public class CheckoutDetails {

	private List<CartItem> cartItems;
	private double deliveryFee;
	private double subTotal;
	private double total;
	private double discount;
	private long totalWeight;
	private int itemCount;
	
//	private String discountNote = "You get 5% discount if purchase is greater than 2000.0";
//	private String deliveryNote = "The minimum delivery fee is 50 pesos. "
//			+ "Anything over 500 grams will be charged an additional 10 pesos per 100gm."
//			+ "Delivery Fee is free if purchase is greater than 5000.0";
//	private String shelfLifeNuts = "All products with nuts have a maximum shelf life of 1 month "
//			+ "(30 days) when kept in a cool dry place.";
//	private String shelfLifeCandies = "All products with just candies have a maximum shelf life of 3 months "
//			+ "(90 days) when kept in a cool dry place.";
	
	public List<CartItem> getCartItems() {
		return cartItems;
	}
	public void setCartItems(List<CartItem> cartItems) {
		this.cartItems = cartItems;
	}
	public double getDeliveryFee() {
		return deliveryFee;
	}
	public void setDeliveryFee(double deliveryFee) {
		this.deliveryFee = deliveryFee;
	}
	public double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public long getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(long totalWeight) {
		this.totalWeight = totalWeight;
	}
//	public String getShelfLifeNuts() {
//		return shelfLifeNuts;
//	}
//	public void setShelfLifeNuts(String shelfLifeNuts) {
//		this.shelfLifeNuts = shelfLifeNuts;
//	}
//	public String getShelfLifeCandies() {
//		return shelfLifeCandies;
//	}
//	public void setShelfLifeCandies(String shelfLifeCandies) {
//		this.shelfLifeCandies = shelfLifeCandies;
//	}
//	public String getDeliveryNote() {
//		return deliveryNote;
//	}
//	public void setDeliveryNote(String deliveryNote) {
//		this.deliveryNote = deliveryNote;
//	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
//	public String getDiscountNote() {
//		return discountNote;
//	}
//	public void setDiscountNote(String discountNote) {
//		this.discountNote = discountNote;
//	}
	public int getItemCount() {
		return itemCount;
	}
	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}
//	@Override
//	public String toString() {
//		return "CheckoutDetails [ cartItems=" + cartItems + ", deliveryFee=" + deliveryFee
//				+ ", subTotal=" + subTotal + ", total=" + total + ", discount=" + discount + ", totalWeight="
//				+ totalWeight + ", itemCount=" + itemCount + ", discountNote=" + discountNote + ", deliveryNote="
//				+ deliveryNote + ", shelfLifeNuts=" + shelfLifeNuts + ", shelfLifeCandies=" + shelfLifeCandies + "]";
//	}
	
	
}
