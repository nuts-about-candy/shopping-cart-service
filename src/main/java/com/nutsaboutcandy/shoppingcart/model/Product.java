package com.nutsaboutcandy.shoppingcart.model;

public class Product {

	private long id;
	private String name;
	private long categoryId;
	private long classificationId;
	private String classification;
	private String category;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public long getClassificationId() {
		return classificationId;
	}

	public void setClassificationId(long classificationId) {
		this.classificationId = classificationId;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", categoryId=" + categoryId + ", classificationId="
				+ classificationId + ", classification=" + classification + ", category=" + category + "]";
	}
	
}
