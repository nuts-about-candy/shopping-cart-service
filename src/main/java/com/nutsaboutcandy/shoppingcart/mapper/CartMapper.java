package com.nutsaboutcandy.shoppingcart.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.nutsaboutcandy.shoppingcart.entity.CartItem;

@Mapper
public interface CartMapper {

	@Select("INSERT INTO CART (user_id, product_id, classification_id, category_id, "
			+ "product_name, classification, category, "
			+ "size_label, prize, weight, "
			+ "quantity, size, total_weight, sub_total) VALUES "
			+ "(#{userId}, #{productId}, #{productDetails.classificationId}, #{productDetails.categoryId}, "
			+ "#{productDetails.name}, #{productDetails.classification}, #{productDetails.category}, "
			+ "#{productSize.label}, #{productSize.prize}, #{productSize.weight},"
			+ "#{quantity}, #{size}, #{totalWeight}, #{subTotal}) "
			+ "RETURNING id")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty ="id")
	long insert(CartItem cartItem);
	
	@Update("UPDATE CART set quantity = #{quantity}, total_weight = #{totalWeight}, "
			+ "sub_total = #{subTotal} "
			+ "WHERE id = #{id} ")
	void update(CartItem cartItem);

	@Delete("DELETE FROM CART WHERE id = #{id}")
	void delete(long id);

	@Select("SELECT * FROM CART "
			+ "WHERE product_id = #{productId} "
			+ "AND user_id = #{userId} AND size = #{size}")
	@Results({
		@Result(column = "id", property = "id"),
		@Result(column = "user_id", property = "userId"),
		@Result(column = "product_id", property = "productId"),
		@Result(column = "product_name", property = "productDetails.name"),
		@Result(column = "classification", property = "productDetails.classification"),
		@Result(column = "category", property = "productDetails.category"),
		@Result(column = "classification_id", property = "productDetails.classificationId"),
		@Result(column = "category_id", property = "productDetails.categoryId"),
		@Result(column = "size_label", property = "productSize.label"),
		@Result(column = "prize", property = "productSize.prize"),
		@Result(column = "weight", property = "productSize.weight"),
		@Result(column = "quantity", property = "quantity"),
		@Result(column = "size", property = "size"),
		@Result(column = "total_weight", property = "totalWeight"),
		@Result(column = "sub_total", property = "subTotal"),
	})
	CartItem findByProductIdAndSize(long productId, long userId, String size);
	
	@Select("SELECT * FROM CART WHERE user_id = #{userId} ORDER BY id")
//			+ "ORDER BY category, classification, product_name")
	@Results({
		@Result(column = "id", property = "id"),
		@Result(column = "user_id", property = "userId"),
		@Result(column = "product_id", property = "productId"),
		@Result(column = "product_name", property = "productDetails.name"),
		@Result(column = "classification", property = "productDetails.classification"),
		@Result(column = "category", property = "productDetails.category"),
		@Result(column = "classification_id", property = "productDetails.classificationId"),
		@Result(column = "category_id", property = "productDetails.categoryId"),
		@Result(column = "size_label", property = "productSize.label"),
		@Result(column = "prize", property = "productSize.prize"),
		@Result(column = "weight", property = "productSize.weight"),
		@Result(column = "quantity", property = "quantity"),
		@Result(column = "size", property = "size"),
		@Result(column = "total_weight", property = "totalWeight"),
		@Result(column = "sub_total", property = "subTotal"),
	})
	List<CartItem> findItemByUserId(long userId);

	@Select("SELECT EXISTS(SELECT 1 FROM CART WHERE id = #{cartId})")
	boolean isCartItemExist(long cartId);

}
