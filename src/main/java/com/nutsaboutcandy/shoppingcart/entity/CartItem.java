package com.nutsaboutcandy.shoppingcart.entity;

import com.nutsaboutcandy.shoppingcart.model.Product;
import com.nutsaboutcandy.shoppingcart.model.ProductSize;

public class CartItem {

	private long id;
	private int userId;
	private int productId;
	private String size;
	private int quantity;

	private double subTotal;
	private long totalWeight;

	private Product productDetails;
	private ProductSize productSize;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public long getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(long totalWeight) {
		this.totalWeight = totalWeight;
	}

	public Product getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(Product productDetails) {
		this.productDetails = productDetails;
	}

	public ProductSize getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSize productSize) {
		this.productSize = productSize;
	}

	@Override
	public String toString() {
		return "CartItem [id=" + id + ", userId=" + userId + ", size=" + size + ", quantity=" + quantity + ", subTotal="
				+ subTotal + ", totalWeight=" + totalWeight + ", productDetails=" + productDetails + ", productSize="
				+ productSize + "]";
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

}
