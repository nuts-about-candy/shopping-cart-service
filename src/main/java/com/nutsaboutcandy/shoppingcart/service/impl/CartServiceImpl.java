package com.nutsaboutcandy.shoppingcart.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.nutsaboutcandy.shoppingcart.entity.CartItem;
import com.nutsaboutcandy.shoppingcart.mapper.CartMapper;
import com.nutsaboutcandy.shoppingcart.model.CheckoutDetails;
import com.nutsaboutcandy.shoppingcart.service.CartService;

@Service
public class CartServiceImpl implements CartService{

	@Autowired
	private CartMapper cartMapper;
	
	@Override
	public CheckoutDetails getCartItemsByUserId(long id) {
		
		CheckoutDetails checkOutDetails = new CheckoutDetails();
		List<CartItem> list = cartMapper.findItemByUserId(id);
		
		double subTotal = list.stream()
				.mapToDouble(item -> item.getSubTotal())
				.sum();
		long totalWeight = list.stream()
				.mapToLong(item -> item.getTotalWeight())
				.sum();
		
		double deliveryFee = 0.0;
		double discount = 0.0;
		if(subTotal < 5000.0) { //DELIVERY FEE IS FREE IF GREATER THAN 5000
			deliveryFee = 50.0;
			if(totalWeight > 500) { // ADDITIONAL DELIVERY FEE IF WEIGHT MORE THAN 500gm
				long excess = (totalWeight - 500) / 100;
				deliveryFee += (excess * 10);
			}
		}
		
		if(subTotal >= 2000.0) {
			discount = subTotal * 0.05;
		}
		
		double total = (subTotal + deliveryFee) - discount;
		checkOutDetails.setItemCount(list.size());
		checkOutDetails.setTotal(total);
		checkOutDetails.setDeliveryFee(deliveryFee);
		checkOutDetails.setDiscount(discount);
		checkOutDetails.setTotalWeight(totalWeight);
		checkOutDetails.setSubTotal(subTotal);
		checkOutDetails.setCartItems(list);
		
		return checkOutDetails;
	}

	@Override
	public void updateCartItem(int cartId, CartItem cartItem) {

		if(cartMapper.isCartItemExist(cartId)) {
			cartMapper.update(cartItem);
		}
		else 
			throw new IllegalArgumentException("Error! Cart Id '"+ cartId +"' not found.");
	}

	@Override
	public void addToCart(CartItem cartItem) {

		CartItem item = cartMapper.findByProductIdAndSize(cartItem.getProductDetails().getId(),
				cartItem.getUserId(), cartItem.getSize());
		if(item != null) { 
			// UPDATE CART ITEM IF SAME PRODUCT AND SIZE EXIST IN CART
			int addedQuantity = item.getQuantity() + cartItem.getQuantity();
			long newTotalWeight = item.getProductSize().getWeight() * addedQuantity;
			double newSubTotal = item.getProductSize().getPrize() * addedQuantity;
			
			item.setQuantity(addedQuantity);
			item.setTotalWeight(newTotalWeight);
			item.setSubTotal(newSubTotal);
			
			cartMapper.update(item);
		}
		else 
			cartMapper.insert(cartItem);
	}

	@Override
	public void addMultipleItemsToCart(int userId, List<CartItem> cartItems) {
		
		if(cartItems.size() <= 0) 
			throw new IllegalArgumentException("Cart items is empty.");
			
		cartItems.forEach(item -> {
			item.setUserId(userId);
			addToCart(item);
		});
	}

	@Override
	public void removeCartItemByCartId(long id) {
		
		if(cartMapper.isCartItemExist(id))
			cartMapper.delete(id);
		else
			throw new IllegalArgumentException("Error! Cart Id '"+ id +"' not found.");
	}

	
}
