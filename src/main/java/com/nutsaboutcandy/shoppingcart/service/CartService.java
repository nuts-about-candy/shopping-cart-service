package com.nutsaboutcandy.shoppingcart.service;

import java.util.List;

import com.nutsaboutcandy.shoppingcart.entity.CartItem;
import com.nutsaboutcandy.shoppingcart.model.CheckoutDetails;

public interface CartService {

	CheckoutDetails getCartItemsByUserId(long id);

	void addToCart(CartItem cartItem);

	void removeCartItemByCartId(long id);

	void addMultipleItemsToCart(int userId, List<CartItem> cartItems);

	void updateCartItem(int cartId, CartItem cartItem);

}
