CREATE TABLE IF NOT EXISTS CART (
 id bigserial not null primary key,
 user_id integer not null,
 
 product_id integer not null,
 classification_id integer not null,
 category_id integer not null,
 product_name varchar(50) not null,
 classification varchar(10) not null,
 category varchar(50) not null,
 
 quantity integer not null,
 size varchar(10) not null,
 
 size_label varchar(20) not null,
 prize real not null,
 weight integer not null,
 
 total_weight integer not null,
 sub_total real not null
);