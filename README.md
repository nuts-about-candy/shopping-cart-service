This Project is a simple e-commerce built using simple Microservice architecture.

The Services are built using Spring Boot 2.
1. user-service : Contains Users and Authentication using Spring Security. It also generates Authorization Token using JWT. 
2. inventory-service : Contains the Categories and Products. It handles product inventory.
3. shopping-cart-service : This service handles Adding and Removing Products to Cart.
4. checkout-service : This service handles checkout process of items in the cart. This also handles the creation of Sales Report.
5. web-app : The web app is built using Angular v9. NodeJS v12 is used.
 -- All services has JWT Authorization enabled.
 
** The web-app communicates with the Services through NGINX.
NGINX Configuration:

server {
    listen       80;
    server_name  localhost;
    location / {
        root   nuts-about-candy;
        index  index.html index.htm;
    }
	location /api/cashier {
        proxy_pass   http://localhost:8001/nutsaboutcandy/api/cashier;
	}
	location /api/inventory {
        proxy_pass   http://localhost:8002/nutsaboutcandy/api/inventory;
	}
	location /api/cart {
        proxy_pass   http://localhost:8003/nutsaboutcandy/api/cart;
	}
	location /api/user {
        proxy_pass   http://localhost:8004/nutsaboutcandy/api/user;
	}
}

** Database used is PostgreSQL v10.
** All services has their own database.
** Database configuration is located in application.properties of the services project.
** Table definition are located in schema-postgresql.sql file.

spring.datasource.driverClassName=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://localhost:5432/nuts_users?characterEncoding=UTF-8 ## DB NAME IS nuts_users
spring.datasource.username=postgres
spring.datasource.password=admin  # CHANGE TO YOUR DATABASE PASSWORD.
spring.datasource.schema=classpath:/schema-postgresql.sql

** IDE Used:
1. Spring Tool Suite
2. VS Code

** Setup
1. Clone and Import the Service Projects as Maven project to your IDE.
2. Make sure all maven dependencies are downloaded.
3. Setup your database based on details on application.properties file. Each service has their own database.
4. All 4 services will run on diff ports, as stated on NGINX config file:
 4.1 user-service runs on port 8004
 4.2 shopping-cart-service runs on port 8003
 4.3 inventory-service runs on port 8002
 4.4 checkout-service runs on port 8001
5. Test Users data will be inserted to the database of user-service during initial run. Located in InventoryServiceApplication.java
6. Test Product data will be inserted to the database of inventory-service during initial run. Located in UserInfoServiceApplication.java
7. Run all services at the same time on your IDE.
8. Clone web-app and open to VS Code.
9. Make sure Nodejs is installed, I use Nodejs v12.16.2. 
10. Install Angular cli "@angular/cli" v9
11. Run "npm install"
12. Run "ng serve -o". If installations are done correctly, this will open a browser tab.


